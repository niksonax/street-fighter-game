import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const firstFighterHealthbar = document.getElementById('left-fighter-indicator');
  const secondFighterHealthbar = document.getElementById('right-fighter-indicator');

  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  let isFirstFighterBlocking = false;
  let isSecondFighterBlocking = false;

  let isFirstFighterComboCooldown = false;
  let isSecondFighterComboCooldown = false;

  let firstFighterCriticalCombo = new Set();
  let secondFighterCriticalCombo = new Set();

  const isSetsEqual = (setA, setB) => setA.size === setB.size && [...setA].every((value) => setB.has(value));

  return new Promise((resolve) => {
    window.addEventListener('keyup', handleKeyup);
    window.addEventListener('keydown', handleKeydown);

    function handleKeyup(event) {
      if (controls.PlayerOneCriticalHitCombination.includes(event.code)) firstFighterCriticalCombo.delete(event.code);
      if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) secondFighterCriticalCombo.delete(event.code);
      releaseFighterFromBlock(event);
    }
    function handleKeydown(event) {
      if (event.repeat) return;
      if (controls.PlayerOneCriticalHitCombination.includes(event.code)) firstFighterCriticalCombo.add(event.code);
      if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) secondFighterCriticalCombo.add(event.code);
      if (isSetsEqual(firstFighterCriticalCombo, new Set(controls.PlayerOneCriticalHitCombination)))
        criticalAttackFighter(secondFighter);
      if (isSetsEqual(secondFighterCriticalCombo, new Set(controls.PlayerTwoCriticalHitCombination)))
        criticalAttackFighter(firstFighter);
      setFighterToBlock(event);
      attackFighter(event);
    }

    function attackFighter(event) {
      if (event.code === controls.PlayerOneAttack && !isFirstFighterBlocking && !isSecondFighterBlocking) {
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
        decreaseHealthbar(secondFighter, secondFighterHealth);
      } else if (event.code === controls.PlayerTwoAttack && !isFirstFighterBlocking && !isSecondFighterBlocking) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        decreaseHealthbar(firstFighter, firstFighterHealth);
      }
      // resolve the promise with the winner when fight is over
      checkForWinner();
    }

    function criticalAttackFighter(defender) {
      if (defender === firstFighter && !isSecondFighterComboCooldown) {
        firstFighterHealth -= getCriticalDamage(secondFighter);
        decreaseHealthbar(firstFighter, firstFighterHealth);
        isSecondFighterComboCooldown = true;
        setTimeout(() => (isSecondFighterComboCooldown = false), 10000);
      } else if (defender === secondFighter && !isFirstFighterComboCooldown) {
        secondFighterHealth -= getCriticalDamage(firstFighter);
        decreaseHealthbar(secondFighter, secondFighterHealth);
        isFirstFighterComboCooldown = true;
        setTimeout(() => (isFirstFighterComboCooldown = false), 10000);
      }
      // resolve the promise with the winner when fight is over
      checkForWinner();
    }

    function setFighterToBlock(event) {
      if (event.code == controls.PlayerOneBlock) {
        isFirstFighterBlocking = true;
      } else if (event.code == controls.PlayerTwoBlock) {
        isSecondFighterBlocking = true;
      }
    }

    function releaseFighterFromBlock(event) {
      if (event.code == controls.PlayerOneBlock) {
        isFirstFighterBlocking = false;
      } else if (event.code == controls.PlayerTwoBlock) {
        isSecondFighterBlocking = false;
      }
    }

    function checkForWinner() {
      if (firstFighterHealth <= 0) resolve(secondFighter);
      if (secondFighterHealth <= 0) resolve(firstFighter);
    }

    function decreaseHealthbar(fighter, health) {
      if (fighter === firstFighter) {
        const firstFighterHealthPercentage = (health / firstFighter.health) * 100;
        firstFighterHealthbar.style.width = `${firstFighterHealthPercentage >= 0 ? firstFighterHealthPercentage : 0}%`;
      } else if (fighter === secondFighter) {
        const secondFighterHealthPercentage = (health / secondFighter.health) * 100;
        secondFighterHealthbar.style.width = `${
          secondFighterHealthPercentage >= 0 ? secondFighterHealthPercentage : 0
        }%`;
      }
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getCriticalDamage(attacker) {
  const damage = getCriticalHitPower(attacker);
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getCriticalHitPower(fighter) {
  const power = fighter.attack * 2;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
