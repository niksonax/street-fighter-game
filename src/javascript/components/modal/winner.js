import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export async function showWinnerModal(fighter) {
  /* const modalBody = createElement({ tag: 'div', className: 'modal-body' });
  modalBody.append(createFighterImage(fighter)); */
  showModal({
    title: `${fighter.name} wins!`,
    bodyElement: createFighterImage(fighter),
    onClose: () => window.location.reload(),
  });
}
