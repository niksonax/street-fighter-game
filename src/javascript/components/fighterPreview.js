import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (fighter == undefined) return '';
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const { name, health, attack, defense } = fighter;
  const fighterName = createElement({
    tagName: 'span',
    className: 'fighter-preview___name',
  });
  fighterName.textContent = name;

  const fighterImage = createFighterImage(fighter);

  const fighterStats = createElement({
    tagName: 'span',
    className: 'fighter-preview___stats',
  });
  fighterStats.innerHTML = `<img src="../resources/heart.png"> ${health}
                            <img src="../resources/swords.png"> ${attack}
                            <img src="../resources/shield.png"> ${defense}`;

  fighterElement.append(fighterName, fighterImage, fighterStats);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
